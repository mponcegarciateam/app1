package myapplication.mario.miprimeraaplicacion;

import android.content.DialogInterface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import android.content.Intent;
import android.app.Activity;
import android.view.View.OnClickListener;
public class MainActivity extends Activity implements OnClickListener {

    private Button boton, boton2;
    private EditText edit;
    private TextView text;
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        boton = (Button) findViewById(R.id.button);
        edit = (EditText) findViewById(R.id.editText);
        text = (TextView) findViewById(R.id.textView);
        boton2 = (Button) findViewById(R.id.button2);
        boton2.setOnClickListener(this);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mensaje = edit.getText().toString();
                text.setText(String.valueOf(mensaje.length()));
            }
        });

    }

    @Override
    public void onClick(View v){

        Intent intent = new Intent(this,Visor.class);
        startActivity(intent);

    }
}
